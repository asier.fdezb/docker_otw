# IMAGE BASE
FROM bash:latest

# Instala SSH y SSHpass
RUN apk update && apk add openssh sshpass



WORKDIR /app

# Clona el repositorio de GitLab
RUN git clone https://gitlab.com/asier.fdezb/automationlevel1to7

# Copia el script Bash desde tu sistema de archivos local al contenedor (Copiar y darle un nombre)
# COPY automation_level1to7.sh automation_level1to7.sh

# Permite que el script sea ejecutable
RUN chmod +x automationlevel1to7/automation_level1to7.sh

# CONFIGURATION THINGS
CMD ["bash","automationlevel1to7/automation_level1to7.sh" ]